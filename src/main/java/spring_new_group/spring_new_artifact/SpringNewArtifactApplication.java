package spring_new_group.spring_new_artifact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringNewArtifactApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringNewArtifactApplication.class, args);
	}

}
